$(document).ready(function () {

    console.log('Document is ready');

    // datepicker
    $(function () {

        var dateFormat = "DD",
            from = $("#from")
                .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
                })
                .on("change", function () {
                    var date2 = $('#from').datepicker('getDate', '+1d');
                    // date2.setDate(date2.getDay()+1);
                    var dateDayfrom = date2.toString().split(' ')[0]
                    console.log(dateDayfrom);
                    $('.psdatepicker__day--from').text(dateDayfrom + ',')
                    
                    to.datepicker("option","minDate", getDate(this));
                }),
            to = $("#to").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
            })
                .on("change", function () {
                    from.datepicker("option", "dayNames", "maxDate", getDate(this));

                    var date3 = $('#to').datepicker('getDate', '+1d');
                    // date3.setDate(date3.getDay()+1);
                    var dateDayTo = date3.toString().split(' ')[0];
                    console.log(dateDayTo.toString().split(' ')[0]);

                    console.log(dateDayTo);
                    $('.psdatepicker__day--to').text(dateDayTo + ',')
                });

        function getDate(element) {
            var date;
            try {
                date = $.datepicker.parseDate(dateFormat, element.value);
            } catch (error) {
                date = null;
            }

            return date;
        }

    });

    // price range slider
    $(function () {

        $("[fsrange]").slider({
            range: true,
            min: 0,
            max: 500,
            values: [0, 200],
            slide: function (event, ui) {
                $("[fsrangeres-min]").html("USD " + "<span class='fsrangeres__num'>" + ui.values[0] + "</span>");
                $("[fsrangeres-max]").html("USD " + "<span class='fsrangeres__num'>" + ui.values[1] + "+" + "</span>");
            }

        });

        $("[fsrangeres-min]").html("USD " + "<span class='fsrangeres__num'>" + $("[fsrange]").slider("values", 0) + "</span>");
        $("[fsrangeres-max]").html("USD " + "<span class='fsrangeres__num'>" + $("[fsrange]").slider("values", 1) + "+" + "</span>");

    });
    // rating range slider
    $(function () {

        var stepCount = 10;
        var steps = "";

        $("[fssteps]").slider({
            range: true,
            min: 1,
            max: stepCount,
            values: [1, 2],
            slide: function (event, ui) {
                // console.log(ui);

            }

        });

        for (var i = 0; i < stepCount; i++) {
            // console.log(i+1);

            var step = "<span>" + parseInt(i + 1) + "</span>";
            steps += step;
            // console.log(step);

            $('[fsrangesteps]').html(steps);

        }

        // $( "[fsrangesteps-min]" ).html("USD " + "<span class='fsrangeres__num'>" + $("[fsrange]").slider( "values", 0 ) + "</span>");
        // $( "[fsrangesteps-max]" ).html("USD " + "<span class='fsrangeres__num'>" + $("[fsrange]").slider( "values", 1 ) + "+" + "</span>");

    });

    // popup functions |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    // svg function for ie
    svg4everybody();
    // svg function for ie

    // filters accordion
    $('[filter-head]').on('click', function () {
        $(this).toggleClass('opened');
        $(this).parent().find('[filter-body]').slideToggle(0)
    });

    // open popups function begin
    $('[data-btn-open]').on('click', function () {
        var popup = $(this).attr('data-btn-open');
        $('[data-popup=' + popup + ']').toggleClass('opened');
        console.log(window.innerWidth);
        
        createOverlay(popup);
        console.log($(this));
    });
    // open popups function end

    // close popups function begin
    $('[data-btn-close]').on('click', function () {
        var popup = $(this).attr('data-btn-close');
        $('[data-popup=' + popup + ']').removeClass('opened');
        $('.poverlay').remove();
        // console.log($(this));
    });
    // close popups function end

// create overlay
    function createOverlay(popup) {
        var overlayLayout = '<div class="poverlay"></div>';
        if(popup === 'mobmenu'){
            $(overlayLayout).prependTo('body').fadeIn(200);
        }else if(window.innerWidth >= 768){
            $(overlayLayout).prependTo($('[data-popup=' + popup + ']')).fadeIn(200);
        }
    }

    $('body').delegate('.poverlay', 'click', function () {
        $(this).remove();
        $('[data-popup]').removeClass('opened');
    });
    // create overlay

    // show hide password
    $("[togglepass]").on('click', function () {

        var input = $($(this).attr("togglepass"));

        if (input.attr("type") == "password") {
            input.attr("type", "text");
            $(this).text("Hide")
        } else {
            input.attr("type", "password");
            $(this).text("Show")
        }
    });

    // load image path to button
    $('[data-load-image]').on('change', function () {
        var publishPhoto = $(this).val();
        $(this).parent().find('[data-publish-image]').text(publishPhoto);
        if ($(this).parent().find('[data-publish-image]').text() == '') {
            $(this).parent().find('[data-publish-image]').text('Publish a photo')
        }
    });

    // popup functions |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    // Fancybox init ================================================================================ //
    var thumbnails = jQuery("a:has(img)").not(".nolightbox").filter(function () {
        return /\.(jpe?g|png|gif|bmp)$/i.test(jQuery(this).attr('href'));
    });
    var fancybox = jQuery('.fancybox');
    fancybox.each(function () {
        jQuery(this).find(thumbnails).attr('data-fancybox', "group-" + fancybox.index(this));
    });
    var fancybox_a = jQuery('.fancybox_a > a');
    fancybox_a.each(function () {
        jQuery(this).find(thumbnails).attr('data-fancybox', "group-" + fancybox.index(this));
    });

    // scroll to top
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 300) {
                $('.scroll-to-top').fadeIn();
            } else {
                $('.scroll-to-top').fadeOut();
            }
        });
        $('.scroll-to-top').click(function () {
            $('body,html').animate({scrollTop: 0}, 400);
            return false;
        });
    });

    // header dropdowns
    function clickElementDropdown(element) {
        $(document).on('click', function (e) {
            if (!element.is(e.target) // element
                && element.has(e.target).length === 0 // childrens
                || $(e.target).parent().hasClass('active') // or if element is active
            ) {
                element.removeClass('active').children('.dropdown').stop().slideUp(300); // click arround element
            } else {
                element.addClass('active').children('.dropdown').stop().slideDown(300); // click on element
            }
        });
    }

    clickElementDropdown($('.lang'));
    clickElementDropdown($('.review'));
    clickElementDropdown($('.login'));

    clickElementDropdown($('.header__lang'));
    clickElementDropdown($('.header__review'));
    clickElementDropdown($('.header__login'));
    clickElementDropdown($('.beach-location__desc__inner--drop-dawn'));

    // header burger menu
    $('.burger-menu').click(function () {
        $(this).toggleClass('active');
        $('.menu-container').slideToggle(); // menu container class
    });

    // burger menu (sub-menu)
    $('.menu-item-has-children>a').click(function (e) {
        if ($(window).width() < 768) { // display width
            e.preventDefault();
            $(this).next('.sub-menu').slideToggle();
        }
    });

    if ($('.recently-viewed')) {
        // main recently viewed
        $('.recently-viewed .slider').slick({
            arrows: true,
            dots: false,
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        arrows: false,
                    },
                },
                {
                    breakpoint: 992,
                    settings: {
                        arrows: false,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 576,
                    settings: {
                        arrows: false,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    },
                },
            ]
        });
        // slider inside slide
        $('.recently-viewed .slider .item-slider, .item-slider').each(function () {
            $(this).slick({
                arrows: false,
                dots: true,
                infinite: false,
                fade: true,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            dots: false,
                            swipe: false,
                        },
                    },
                ]
            });
        });
    }

    $('.beach-recently-viewed > .slider-beach').not('.slick-initialized').slick({
        arrows: true,
        dots: false,
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    arrows: false,
                },
            },
            {
                breakpoint: 992,
                settings: {
                    arrows: false,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                },
            },
            {
                breakpoint: 576,
                settings: {
                    arrows: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ]
    });

    $('.beach-recently-viewed > .slider-beach .item-slider, .item-slider').each(function () {
        $(this).not('.slick-initialized').slick({
            arrows: false,
            dots: true,
            infinite: false,
            fade: true,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        dots: false,
                        swipe: false,
                    },
                },
            ]
        });
    });

    // init pc slider
    $('.beach-desc__overview__slider').not('.slick-initialized').slick({
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        cssEase: 'linear',
        infinite: false,
        prevArrow: $('.beach-desc__overview__slider-controls__arrow--left'),
        nextArrow: $('.beach-desc__overview__slider-controls__arrow--right'),
    });

    $(window).resize(function(e) {
        if ($(window).width() <= 1024) {
            console.log('<= 1024');
            $('.beach-desc__sliderMob__slider').slick({
                dots: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                cssEase: 'linear',
                infinite: false,
                prevArrow: $('.beach-desc__sliderMob__slider__controls__arrow--prev'),
                nextArrow: $('.beach-desc__sliderMob__slider__controls__arrow--next'),
                responsive: [
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
            // $('.beach-desc__overview__slider').slick('unslick');
        }
        if($(window).width() > 1024) {
            console.log('> 1024');
            $('.beach-desc__overview__slider').slick({
                dots: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                cssEase: 'linear',
                infinite: false,
                prevArrow: $('.beach-desc__overview__slider-controls__arrow--left'),
                nextArrow: $('.beach-desc__overview__slider-controls__arrow--right'),
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
            // $('.beach-desc__sliderMob__slider').slick('unslick');
        }
    });

    $('.beach-desc__sliderMob__slider').not('.slick-initialized').slick({
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        cssEase: 'linear',
        infinite: false,
        prevArrow: $('.beach-desc__sliderMob__slider__controls__arrow--prev'),
        nextArrow: $('.beach-desc__sliderMob__slider__controls__arrow--next'),
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });


    // block-1
    $('.block-1 .slider').slick({
        arrows: false,
        dots: false,
        slidesToShow: 8,
        slidesToScroll: 1,
        infinite: false,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 5,
                },
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 4,
                },
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                },
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                },
            },
        ]
    });

    // block-2
    if ($(window).width() < 768) {
        $('.block-2 .slider').slick({
            arrows: false,
            dots: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: false,
        });
    }

    // card image slider component
    $('[card-img-slider]').slick({
        arrows: false,
        slidesToShow: 1,
        infinite: false,
        slidesToScroll: 1,
        dots: true
    });

    // tabs
    $('ul.beach-tabs__head').on('click', 'li:not(.beach-tabs__head__item--active)', function () {
        $(this)
            .addClass('beach-tabs__head__item--active').siblings().removeClass('beach-tabs__head__item--active')
            .closest('div.beach-tabs__wrapper').find('div.beach-tabs__content').removeClass('beach-tabs__content--active').eq($(this).index()).addClass('beach-tabs__content--active');
    });

    // show more btn for reviews
    $('.beach-desc__overviewMob__show').on('click', function (e){
        e.preventDefault();
        $('.beach-desc__overviewMob__item__review').css({'max-height' : '100%'})
    });

    // show more btn for reviews list mob
    $('.beach-tabs__content__item__link--more a').on('click', function (e){
        e.preventDefault();
        $(this).parent().parent().parent().find($(".beach-tabs__content__item__text")).css({'max-height' : '100%'})
    })

    // show more btn for about mob
    $('.beach-about__link a').on('click', function (e){
        e.preventDefault();
        $(this).parent().parent().find($('.beach-about__inner')).css({'max-height' : '100%'})
    })

});